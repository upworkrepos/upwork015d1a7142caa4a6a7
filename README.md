## packer templates are in different branches

## Links with jaeger samples
- [jaeger deployment documentation](https://github.com/jaegertracing/documentation/blob/master/content/docs/1.8/deployment.md)
- [jaeger storage documentation](https://github.com/jaegertracing/jaeger/blob/master/plugin/storage/es/README.md)
- [jaeger sample environment](https://github.com/jaegertracing/jaeger/blob/master/docker-compose/jaeger-docker-compose.yml)

## jaeger command line arguments to environment vars conversion rules

```text
[root@localhost ~]# /usr/local/bin/jaeger-query env

All command line options can be provided via environment variables by converting
their names to upper case and replacing punctuation with underscores. For example,

      command line option                 environment variable
      ------------------------------------------------------------------
      --cassandra.connections-per-host    CASSANDRA_CONNECTIONS_PER_HOST
      --metrics-backend                   METRICS_BACKEND

The following configuration options are only available via environment variables:

      SPAN_STORAGE_TYPE string         The type of backend (cassandra, elasticsearch, kafka, memory) used for trace storage. Multiple backends can be specified (currently only for writing spans) as comma-separated list, e.g. "cassandra,kafka". (default "cassandra")
      DEPENDENCY_STORAGE_TYPE string   The type of backend used for service dependencies storage. (default "${SPAN_STORAGE}")
```
## jaeger-collector command line arguments

```text
[root@localhost ~]# /usr/local/bin/jaeger-collector help
Jaeger collector receives traces from Jaeger agents and runs them through a processing pipeline.

Usage:
  jaeger-collector [flags]
  jaeger-collector [command]

Available Commands:
  env         Help about environment variables
  help        Help about any command
  version     Print the version

Flags:
      --cassandra-archive.connections-per-host int      The number of Cassandra connections from a single backend instance
      --cassandra-archive.consistency string            The Cassandra consistency level, e.g. ANY, ONE, TWO, THREE, QUORUM, ALL, LOCAL_QUORUM, EACH_QUORUM, LOCAL_ONE (default LOCAL_ONE)
      --cassandra-archive.enabled                       Enable extra storage
      --cassandra-archive.keyspace string               The Cassandra keyspace for Jaeger data
      --cassandra-archive.max-retry-attempts int        The number of attempts when reading from Cassandra
      --cassandra-archive.password string               Password for password authentication for Cassandra
      --cassandra-archive.port int                      The port for cassandra
      --cassandra-archive.proto-version int             The Cassandra protocol version
      --cassandra-archive.reconnect-interval duration   Reconnect interval to retry connecting to downed hosts (default 0s)
      --cassandra-archive.servers string                The comma-separated list of Cassandra servers
      --cassandra-archive.socket-keep-alive duration    Cassandra's keepalive period to use, enabled if > 0 (default 0s)
      --cassandra-archive.timeout duration              Timeout used for queries. A Timeout of zero means no timeout (default 0s)
      --cassandra-archive.tls                           Enable TLS
      --cassandra-archive.tls.ca string                 Path to TLS CA file
      --cassandra-archive.tls.cert string               Path to TLS certificate file
      --cassandra-archive.tls.key string                Path to TLS key file
      --cassandra-archive.tls.server-name string        Override the TLS server name
      --cassandra-archive.tls.verify-host               Enable (or disable) host key verification
      --cassandra-archive.username string               Username for password authentication for Cassandra
      --cassandra.connections-per-host int              The number of Cassandra connections from a single backend instance (default 2)
      --cassandra.consistency string                    The Cassandra consistency level, e.g. ANY, ONE, TWO, THREE, QUORUM, ALL, LOCAL_QUORUM, EACH_QUORUM, LOCAL_ONE (default LOCAL_ONE)
      --cassandra.keyspace string                       The Cassandra keyspace for Jaeger data (default "jaeger_v1_test")
      --cassandra.max-retry-attempts int                The number of attempts when reading from Cassandra (default 3)
      --cassandra.password string                       Password for password authentication for Cassandra
      --cassandra.port int                              The port for cassandra
      --cassandra.proto-version int                     The Cassandra protocol version (default 4)
      --cassandra.reconnect-interval duration           Reconnect interval to retry connecting to downed hosts (default 1m0s)
      --cassandra.servers string                        The comma-separated list of Cassandra servers (default "127.0.0.1")
      --cassandra.socket-keep-alive duration            Cassandra's keepalive period to use, enabled if > 0 (default 0s)
      --cassandra.span-store-write-cache-ttl duration   The duration to wait before rewriting an existing service or operation name (default 12h0m0s)
      --cassandra.timeout duration                      Timeout used for queries. A Timeout of zero means no timeout (default 0s)
      --cassandra.tls                                   Enable TLS
      --cassandra.tls.ca string                         Path to TLS CA file
      --cassandra.tls.cert string                       Path to TLS certificate file
      --cassandra.tls.key string                        Path to TLS key file
      --cassandra.tls.server-name string                Override the TLS server name
      --cassandra.tls.verify-host                       Enable (or disable) host key verification (default true)
      --cassandra.username string                       Username for password authentication for Cassandra
      --collector.http-port int                         The http port for the collector service (default 14268)
      --collector.num-workers int                       The number of workers pulling items from the queue (default 50)
      --collector.port int                              The tchannel port for the collector service (default 14267)
      --collector.queue-size int                        The queue size of the collector (default 2000)
      --collector.zipkin.http-port int                  The http port for the Zipkin collector service e.g. 9411
      --config-file string                              Configuration file in JSON, TOML, YAML, HCL, or Java properties formats (default none). See spf13/viper for precedence.
      --health-check-http-port int                      The http port for the health check service (default 14269)
  -h, --help                                            help for jaeger-collector
      --log-level string                                Minimal allowed log Level. For more levels see https://github.com/uber-go/zap (default "info")
      --metrics-backend string                          Defines which metrics backend to use for metrics reporting: expvar, prometheus, none (default "prometheus")
      --metrics-http-route string                       Defines the route of HTTP endpoint for metrics backends that support scraping (default "/metrics")
      --sampling.strategies-file string                 The path for the sampling strategies file in JSON format. See sampling documentation to see format of the file
      --span-storage.type string                        Deprecated; please use SPAN_STORAGE_TYPE environment variable. Run this binary with "env" command for help.

Use "jaeger-collector [command] --help" for more information about a command.
```
## jaeger-ingester command line arguments

```text
[root@localhost ~]# /usr/local/bin/jaeger-ingester help
Jaeger ingester consumes spans from a particular Kafka topic and writes them to all configured storage types.

Usage:
  (experimental) jaeger-ingester [flags]
  (experimental) [command]

Available Commands:
  env         Help about environment variables
  help        Help about any command
  version     Print the version

Flags:
      --cassandra-archive.connections-per-host int      The number of Cassandra connections from a single backend instance
      --cassandra-archive.consistency string            The Cassandra consistency level, e.g. ANY, ONE, TWO, THREE, QUORUM, ALL, LOCAL_QUORUM, EACH_QUORUM, LOCAL_ONE (default LOCAL_ONE)
      --cassandra-archive.enabled                       Enable extra storage
      --cassandra-archive.keyspace string               The Cassandra keyspace for Jaeger data
      --cassandra-archive.max-retry-attempts int        The number of attempts when reading from Cassandra
      --cassandra-archive.password string               Password for password authentication for Cassandra
      --cassandra-archive.port int                      The port for cassandra
      --cassandra-archive.proto-version int             The Cassandra protocol version
      --cassandra-archive.reconnect-interval duration   Reconnect interval to retry connecting to downed hosts (default 0s)
      --cassandra-archive.servers string                The comma-separated list of Cassandra servers
      --cassandra-archive.socket-keep-alive duration    Cassandra's keepalive period to use, enabled if > 0 (default 0s)
      --cassandra-archive.timeout duration              Timeout used for queries. A Timeout of zero means no timeout (default 0s)
      --cassandra-archive.tls                           Enable TLS
      --cassandra-archive.tls.ca string                 Path to TLS CA file
      --cassandra-archive.tls.cert string               Path to TLS certificate file
      --cassandra-archive.tls.key string                Path to TLS key file
      --cassandra-archive.tls.server-name string        Override the TLS server name
      --cassandra-archive.tls.verify-host               Enable (or disable) host key verification
      --cassandra-archive.username string               Username for password authentication for Cassandra
      --cassandra.connections-per-host int              The number of Cassandra connections from a single backend instance (default 2)
      --cassandra.consistency string                    The Cassandra consistency level, e.g. ANY, ONE, TWO, THREE, QUORUM, ALL, LOCAL_QUORUM, EACH_QUORUM, LOCAL_ONE (default LOCAL_ONE)
      --cassandra.keyspace string                       The Cassandra keyspace for Jaeger data (default "jaeger_v1_test")
      --cassandra.max-retry-attempts int                The number of attempts when reading from Cassandra (default 3)
      --cassandra.password string                       Password for password authentication for Cassandra
      --cassandra.port int                              The port for cassandra
      --cassandra.proto-version int                     The Cassandra protocol version (default 4)
      --cassandra.reconnect-interval duration           Reconnect interval to retry connecting to downed hosts (default 1m0s)
      --cassandra.servers string                        The comma-separated list of Cassandra servers (default "127.0.0.1")
      --cassandra.socket-keep-alive duration            Cassandra's keepalive period to use, enabled if > 0 (default 0s)
      --cassandra.span-store-write-cache-ttl duration   The duration to wait before rewriting an existing service or operation name (default 12h0m0s)
      --cassandra.timeout duration                      Timeout used for queries. A Timeout of zero means no timeout (default 0s)
      --cassandra.tls                                   Enable TLS
      --cassandra.tls.ca string                         Path to TLS CA file
      --cassandra.tls.cert string                       Path to TLS certificate file
      --cassandra.tls.key string                        Path to TLS key file
      --cassandra.tls.server-name string                Override the TLS server name
      --cassandra.tls.verify-host                       Enable (or disable) host key verification (default true)
      --cassandra.username string                       Username for password authentication for Cassandra
      --config-file string                              Configuration file in JSON, TOML, YAML, HCL, or Java properties formats (default none). See spf13/viper for precedence.
      --health-check-http-port int                      The http port for the health check service (default 14270)
  -h, --help                                            help for (experimental)
      --ingester.deadlockInterval duration              Interval to check for deadlocks. If no messages gets processed in given time, ingester app will exit. Value of 0 disables deadlock check. (default 1m0s)
      --ingester.http-port int                          The http port for the ingester service (default 14271)
      --ingester.parallelism string                     The number of messages to process in parallel (default "1000")
      --kafka.brokers string                            The comma-separated list of kafka brokers. i.e. '127.0.0.1:9092,0.0.0:1234' (default "127.0.0.1:9092")
      --kafka.encoding string                           The encoding of spans ("protobuf" or "json") consumed from kafka (default "protobuf")
      --kafka.group-id string                           The Consumer Group that ingester will be consuming on behalf of (default "jaeger-ingester")
      --kafka.topic string                              The name of the kafka topic to consume from (default "jaeger-spans")
      --log-level string                                Minimal allowed log Level. For more levels see https://github.com/uber-go/zap (default "info")
      --metrics-backend string                          Defines which metrics backend to use for metrics reporting: expvar, prometheus, none (default "prometheus")
      --metrics-http-route string                       Defines the route of HTTP endpoint for metrics backends that support scraping (default "/metrics")
      --span-storage.type string                        Deprecated; please use SPAN_STORAGE_TYPE environment variable. Run this binary with "env" command for help.

Use "(experimental) [command] --help" for more information about a command.
```
# jaeger-query command line arguments

```text
[root@localhost ~]# /usr/local/bin/jaeger-query help
Jaeger query service provides a Web UI and an API for accessing trace data.

Usage:
  jaeger-query [flags]
  jaeger-query [command]

Available Commands:
  env         Help about environment variables
  help        Help about any command
  version     Print the version

Flags:
      --cassandra-archive.connections-per-host int      The number of Cassandra connections from a single backend instance
      --cassandra-archive.consistency string            The Cassandra consistency level, e.g. ANY, ONE, TWO, THREE, QUORUM, ALL, LOCAL_QUORUM, EACH_QUORUM, LOCAL_ONE (default LOCAL_ONE)
      --cassandra-archive.enabled                       Enable extra storage
      --cassandra-archive.keyspace string               The Cassandra keyspace for Jaeger data
      --cassandra-archive.max-retry-attempts int        The number of attempts when reading from Cassandra
      --cassandra-archive.password string               Password for password authentication for Cassandra
      --cassandra-archive.port int                      The port for cassandra
      --cassandra-archive.proto-version int             The Cassandra protocol version
      --cassandra-archive.reconnect-interval duration   Reconnect interval to retry connecting to downed hosts (default 0s)
      --cassandra-archive.servers string                The comma-separated list of Cassandra servers
      --cassandra-archive.socket-keep-alive duration    Cassandra's keepalive period to use, enabled if > 0 (default 0s)
      --cassandra-archive.timeout duration              Timeout used for queries. A Timeout of zero means no timeout (default 0s)
      --cassandra-archive.tls                           Enable TLS
      --cassandra-archive.tls.ca string                 Path to TLS CA file
      --cassandra-archive.tls.cert string               Path to TLS certificate file
      --cassandra-archive.tls.key string                Path to TLS key file
      --cassandra-archive.tls.server-name string        Override the TLS server name
      --cassandra-archive.tls.verify-host               Enable (or disable) host key verification
      --cassandra-archive.username string               Username for password authentication for Cassandra
      --cassandra.connections-per-host int              The number of Cassandra connections from a single backend instance (default 2)
      --cassandra.consistency string                    The Cassandra consistency level, e.g. ANY, ONE, TWO, THREE, QUORUM, ALL, LOCAL_QUORUM, EACH_QUORUM, LOCAL_ONE (default LOCAL_ONE)
      --cassandra.keyspace string                       The Cassandra keyspace for Jaeger data (default "jaeger_v1_test")
      --cassandra.max-retry-attempts int                The number of attempts when reading from Cassandra (default 3)
      --cassandra.password string                       Password for password authentication for Cassandra
      --cassandra.port int                              The port for cassandra
      --cassandra.proto-version int                     The Cassandra protocol version (default 4)
      --cassandra.reconnect-interval duration           Reconnect interval to retry connecting to downed hosts (default 1m0s)
      --cassandra.servers string                        The comma-separated list of Cassandra servers (default "127.0.0.1")
      --cassandra.socket-keep-alive duration            Cassandra's keepalive period to use, enabled if > 0 (default 0s)
      --cassandra.span-store-write-cache-ttl duration   The duration to wait before rewriting an existing service or operation name (default 12h0m0s)
      --cassandra.timeout duration                      Timeout used for queries. A Timeout of zero means no timeout (default 0s)
      --cassandra.tls                                   Enable TLS
      --cassandra.tls.ca string                         Path to TLS CA file
      --cassandra.tls.cert string                       Path to TLS certificate file
      --cassandra.tls.key string                        Path to TLS key file
      --cassandra.tls.server-name string                Override the TLS server name
      --cassandra.tls.verify-host                       Enable (or disable) host key verification (default true)
      --cassandra.username string                       Username for password authentication for Cassandra
      --config-file string                              Configuration file in JSON, TOML, YAML, HCL, or Java properties formats (default none). See spf13/viper for precedence.
      --health-check-http-port int                      The http port for the health check service (default 16687)
  -h, --help                                            help for jaeger-query
      --log-level string                                Minimal allowed log Level. For more levels see https://github.com/uber-go/zap (default "info")
      --metrics-backend string                          Defines which metrics backend to use for metrics reporting: expvar, prometheus, none (default "prometheus")
      --metrics-http-route string                       Defines the route of HTTP endpoint for metrics backends that support scraping (default "/metrics")
      --query.base-path string                          The base path for all HTTP routes, e.g. /jaeger; useful when running behind a reverse proxy (default "/")
      --query.port int                                  The port for the query service (default 16686)
      --query.static-files string                       The directory path override for the static assets for the UI
      --query.ui-config string                          The path to the UI configuration file in JSON format
      --span-storage.type string                        Deprecated; please use SPAN_STORAGE_TYPE environment variable. Run this binary with "env" command for help.

Use "jaeger-query [command] --help" for more information about a command.
```